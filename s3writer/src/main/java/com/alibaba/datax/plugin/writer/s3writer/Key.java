package com.alibaba.datax.plugin.writer.s3writer;

/**
 * Created by boomlee on 2022/7/22.
 */
public class Key {
    public static final String REGION = "region";

    public static final String ENDPOINT = "endpoint";

    public static final String ACCESSID = "accessId";

    public static final String ACCESSKEY = "accessKey";

    public static final String BUCKET = "bucket";

    public static final String OBJECT = "object";

}
