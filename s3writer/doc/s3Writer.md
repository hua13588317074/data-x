# S3Writer 插件文档

---

## 1.快速介绍
Amazon Simple Storage Service (Amazon S3) 是一种对象存储服务，提供行业领先的可扩展性、数据可用性、安全性和性能。各种规模和行业的客户可以为几乎任何使用案例存储和保护任意数量的数据，例如数据湖、云原生应用程序和移动应用程序。通过经济高效的存储类和易于使用的管理功能，您可以优化成本、组织数据并配置精细调整过的访问控制，从而满足特定的业务、组织和合规性要求。

## 2.实现原理
S3Writer 通过 DataX 框架获取 Reader 生成的协议数据，切分成CSV文件（自定义分隔符） 导入到 S3 中。

## 3.功能说明
### 3.1 配置样例
```
"writer": {
          "name": "s3writer",
          "parameter": {
            "endpoint": "https://boomlee.s3.ap-southeast-1.amazonaws.com/test/",
            "accessId": "your_access_id",
            "accessKey":"your_access_key",
            "bucket":"bucket_name",
            "object":"object_name",
            "encoding":"utf-8",
            "fieldDelimiter":"分隔符",
            "writeMode":"append",
            "region":"region_name",
          }
        }
      }
    ]
  }
}
```
### 3.2 参数说明
| 参数 | 类型 | 描述 |
| ---- | ---- | ---- |
| endpoint | string | S3 地址 |
| accessId | string | S3 访问 ID |
| accessKey | string | S3 访问 Key |
| bucket | string | S3 bucket 名称 |
| object | string | S3 object 名称 |
| encoding | string | 文件编码 |
| fieldDelimiter | string | 分隔符 |
| writeMode | string | 写入模式 |
| region | string | 地域 |

写入模式 writeMode 说明
* append: 写入前不做任何处理,filename_UUID拼装生成，文件名不冲突。
* truncate:写入前清理目录下一fileName前缀的所有文件。
* nonConflict: 如果目录下有fileName前缀的文件，直接报错。

更多使用案例关注博客 https://blog.csdn.net/BoomLee

