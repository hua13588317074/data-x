# DataX

DataX 是阿里巴巴集团内被广泛使用的离线数据同步工具/平台，实现包括 MySQL、Oracle、SqlServer、Postgre、HDFS、Hive、ADS、HBase、TableStore(OTS)、MaxCompute(ODPS)、DRDS 等各种异构数据源之间高效的数据同步功能。


# Features

DataX本身作为数据同步框架，将不同数据源的同步抽象为从源头数据源读取数据的Reader插件，以及向目标端写入数据的Writer插件，理论上DataX框架可以支持任意数据源类型的数据同步工作。同时DataX插件体系作为一套生态系统, 每接入一套新数据源该新加入的数据源即可实现和现有的数据源互通。


# DataX详细介绍

##### 请参考：[DataX-Introduction](https://github.com/alibaba/DataX/blob/master/introduction.md)

# 推荐一个好用的梯子
https://sockboom.shop/auth/register?affid=176961

# 版本更新
    1.添加 GreenPlum gpdbwriter,使用 COPY 进行数据写入，速度大幅度提升
    2.添加 Sqlserver2000 sqlserverreader2000 插件，支持sqlserver2000数据库读取
    3.添加 Tbase TbaseWriter 插件，支持腾讯Tbase数据库,使用copy写入、速度大幅度提升
    4.添加 Amazon S3 S3Writer 插件
    5.添加 Oracle XMLType 解析
    6.更新字符串切分任务，提升速度，json中添加
       "splitPk": "name",
       "pkType":"pkTypeString",
    7.hdfs writer 支持 parquet 




# 本地调试
    moudle:data-core com.alibaba.datax.core.Engine
    203行 打开注释，指定执行 target 和执行 json 运行即可

更多大数据技术，欢迎关注博客
https://blog.csdn.net/BoomLee
