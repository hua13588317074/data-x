package com.alibaba.datax.core.transport.transformer;

import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.alibaba.datax.common.element.Record;
import com.alibaba.datax.common.element.StringColumn;
import com.alibaba.datax.transformer.Transformer;


/**
 * @author Boomlee
 * @description 解密
 * @date 2023/3/20 17:24
 */

public class DecryptTransformer extends Transformer {

    public DecryptTransformer() {
        setTransformerName("dx_decrypt");
    }


    @Override
    public Record evaluate(Record record, Object... paras) {

        String key;
        key = (String) paras[1];
        SymmetricCrypto sm4 = new SymmetricCrypto("SM4/ECB/PKCS5Padding", key.getBytes());

        int columnCount = record.getColumnNumber();
        for (int i = 0; i < columnCount; i++) {
            record.setColumn(i, new StringColumn(sm4.decryptStr(record.getColumn(i).asString())));
        }
        return record;
    }
}
