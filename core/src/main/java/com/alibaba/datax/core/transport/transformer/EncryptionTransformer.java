package com.alibaba.datax.core.transport.transformer;

import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.alibaba.datax.common.element.Record;
import com.alibaba.datax.common.element.StringColumn;
import com.alibaba.datax.transformer.Transformer;


/**
 * @author boomlee
 * @desc 加密
 */
public class EncryptionTransformer extends Transformer {

    public EncryptionTransformer() {
        setTransformerName("dx_encrypt");
    }


    @Override
    public Record evaluate(Record record, Object... paras) {

        String key;

        key = (String) paras[1];

       SymmetricCrypto sm4 = new SymmetricCrypto("SM4/ECB/PKCS5Padding", key.getBytes());

        //加密后所有字段均为字符串
        int columnCount = record.getColumnNumber();
        for (int i = 0; i < columnCount; i++) {
            record.setColumn(i, new StringColumn(sm4.encryptHex(record.getColumn(i).asString())));
        }
        return record;
    }

}
